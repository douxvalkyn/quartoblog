### Documentation pour utiliser Quarto sur Gitlab:

<https://cedric.midoux.pages.mia.inra.fr/slides/posts/2022-12-08_quartoblog/>

### Aide pour déployer un website quarto sur gitlab:

Dans la console RStudio:

`usethis::use_git() usethis::use_git_remote(name = "origin", url = "git@gitlab.com:douxvalkyn/quartoblog.git")`

Dans un Terminal:

`$ git push -u origin master`
